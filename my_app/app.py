from flask import Flask
app = Flask(__name__)

@app.route("/")
def read_file():
    f = open('data.csv')
    line = f.readline()
    while line:
        print line
        line = f.readline()
    return "Finished reading file"

if __name__ == "__main__":
    app.run()
